var map;
function initMap() {
  // The location of thessaloniki
  var coords = {
    lat: 40.635852,
    lng: 22.944206
  };
  // The map, centered at Uluru
  map = new google.maps.Map(document.getElementById('map'), {
    zoom: 15,
    center: coords
  });

  var infowindow = new google.maps.InfoWindow();
  //  var marker = new google.maps.Marker({position: uluru, map: map});
  if (navigator.geolocation) {
    navigator.geolocation.getCurrentPosition(function(position) {
      var pos = {
        lat: position.coords.latitude,
        lng: position.coords.longitude
      };
      marker = new google.maps.Marker({
        position: new google.maps.LatLng(pos.lat, pos.lng),
        map: map
      });
      map.setCenter(pos);
      marker.setIcon('http://maps.google.com/mapfiles/ms/icons/blue-dot.png')
      marker.addListener('click', function() {
        infowindow.setContent('Η τοποθεσία μου');
        infowindow.open(map, marker);
      });
    }, function() {
      handleLocationError(true, marker, map.getCenter());
    });
    } else {
    // Browser doesn't support Geolocation
    handleLocationError(false, marker, map.getCenter());
  }
}
