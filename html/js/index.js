const request = superagent
const URL = 'https://getnearpharmacies.com/api'

async function getPharmacies () {
  const res = await request.get(URL).set('Access-Control-Allow-Origin', '*')
  return res.body
}

async function createTable() {
    console.log('before')
    pharmacies = await getPharmacies()
    console.log(pharmacies)

    var table = document.createElement('table')
    var tableBody = document.createElement('tbody')
    table.appendChild(tableBody);

    let header = ['Name', 'Address', 'Distance in km', 'Duration in min', 'Closes in']

    var tr = document.createElement('tr');
    tableBody.appendChild(tr);

    for (i = 0; i < header.length; i++) {
        var th = document.createElement('th')
        th.width = '75';
        th.appendChild(document.createTextNode(header[i]));
        tr.appendChild(th);
    }

    var infowindow = new google.maps.InfoWindow();
    var marker;

    for (i = 0; i < pharmacies.length; i++) {
        let tr = document.createElement('tr');
        for (let key of ['name', 'address', 'distance', 'duration', 'closesIn']) {
            let td = document.createElement('td')
            td.appendChild(document.createTextNode(pharmacies[i][key]))
            tr.appendChild(td)
        }
        tableBody.appendChild(tr);

        marker = new google.maps.Marker({
          position: new google.maps.LatLng(pharmacies[i].coordinates),
          map: map,
          name: pharmacies[i].name,
      });

      google.maps.event.addListener(marker, 'click', (function(marker, i) {
          return function() {
            infowindow.setContent(marker.name);
            infowindow.open(map, marker);
          }
      })(marker, i));
    }

    return table
}

createTable().then((table) => {
  let myTableDiv = document.getElementById('table-container')

  myTableDiv.appendChild(table)
})
