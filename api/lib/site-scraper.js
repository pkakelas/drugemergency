const rp = require('request-promise')
const nodeHtmlParser = require('node-html-parser')
const { parse } = nodeHtmlParser
const setRelativeTime = require('./datetime')

const PHARMACIES_URL = 'https://www.vrisko.gr/efimeries-farmakeion/thessaloniki'

const dataToClasses = {
  'name': '.ResultName',
  'address': '.ResultAddr',
  'times': '.DutyTimes .firstTime',
  'active': '.DutyActive span',
}

let parsePharmacies = (root) => {
  let pharmacies = []
  let counter = 0

  for (pharmacy of root.querySelectorAll('.DutiesResult')) {
    let pharmacyData = {}

    for (element of Object.keys(dataToClasses)) {
      data = getElement(pharmacy, element)

      if (data !== '') {
        pharmacyData[element] = data
      }

    }

    pharmacies.push(pharmacyData)
  }

  setRelativeTime(pharmacies)

  return pharmacies
}

let getPharmaciesHTML = async () => {
  let options = {
    method: 'GET',
    uri: PHARMACIES_URL
  }

  return await rp(options)
}

let getParsedSite = async () => {
  requestResult = await getPharmaciesHTML()

  return parse(requestResult)
}

let filterActivePharmacies = (pharmacies) => {
  return pharmacies.filter((pharmacy) => {
    return pharmacy.active !== undefined 
  })
}

let getElement = (pharmacy, element) => {
  data = pharmacy.querySelector(dataToClasses[element])
  if (data) {
    return data.text.trim()
  }

  return undefined
}

let getPharmacies = async () => {
  const root = await getParsedSite()
  const pharmacies = parsePharmacies(root)

  return filterActivePharmacies(pharmacies)
}

module.exports = getPharmacies
