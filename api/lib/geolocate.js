const rp = require('request-promise')
const { getParsedLocations, getParsedLocation } = require('./helpers')

const KEY = 'AIzaSyDkq_8fuYEw5jgyeIAaU792K6-LrG-S7eY'
const URL = 'https://maps.googleapis.com/maps/api/distancematrix/json'
const GEOCODE_URL = 'https://maps.googleapis.com/maps/api/geocode/json'
const myLocation = [40.5988529, 22.9408882]
const HOW_MANY_PER_TIME = 25

const addGeolocation = async (pharmacies) => {
  for (i = 0; i <= pharmacies.length; i += HOW_MANY_PER_TIME) {
    await geolocateBatch(pharmacies, i, i + HOW_MANY_PER_TIME)
  }
}

const geolocateBatch = async (pharmacies, start, end) => {
  const addresses = pharmacies.slice(start, end).map(p => p.address)
  let geolocated;

  try {
    geolocated = await getDistanceMatrix(myLocation, addresses)
  }
  catch (e) {
    console.log(`Geolocate error on batch ${start} - ${end}`, e.message) 
    return
  }

  if (geolocated.rows[0] == undefined) {
    return
  }

  const distances = geolocated.rows[0].elements.map((target) => {
    return {distance: target.distance.text, duration: target.duration.text}
  })

  counter = 0
  for (let pharmacy of pharmacies.slice(start, end)) {
    pharmacy.distance = parseFloat(distances[counter].distance)
    pharmacy.duration = parseFloat(distances[counter].duration)

    ++counter
  }
}

const getDistanceMatrix = async (myLocation, otherLocations) => {
  const options = {
    method: 'GET',
    uri: URL,
    qs: {
      origins: myLocation.join(','),
      destinations: getParsedLocations(otherLocations),
      mode: 'driving',
      key: KEY
    },
    json: true
  }

  console.log(options)

  return await rp(options)
}

const addLngLat = async (pharmacies) => {
  for (pharmacy of pharmacies) {
    const options = {
      method: 'GET',
      uri: GEOCODE_URL,
      qs: {
        address: getParsedLocation(pharmacy.address),
        key: KEY
      },
      json: true
    }

    const res = await rp(options)

    pharmacy.coordinates = res.results[0].geometry.location
  }
}

module.exports = {
  addGeolocation: addGeolocation,
  addLngLat: addLngLat
}
