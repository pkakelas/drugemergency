const sortBy = (pharmacies, element) => {
  return pharmacies.sort((a, b) => a[element] - b[element]);
}

const getParsedLocations = (locations) => {
  return locations.map(location => location.replace(',', '').replace('&', ' ').replace('-', ' ').split(' ').join('+')).join('|')
}

const getParsedLocation = (location) => {
  return location.replace(',', '').replace('&', ' ').replace('-', ' ').split(' ').join('+')
}

module.exports = {
  sortBy: sortBy,
  getParsedLocations: getParsedLocations,
  getParsedLocation: getParsedLocation
}
