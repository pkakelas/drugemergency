const moment = require('moment')

let getCloseTime = (range) => {
  return range.split('-')[1]
}

let getRelativeTime = (timeRange) => {
  return moment(getCloseTime(timeRange), "hh:mm").fromNow()
}

module.exports = (pharmacies) => {
  updatedPharmacies = []

  for (pharmacy of pharmacies) {
    pharmacy['closesIn'] = getRelativeTime(pharmacy['times'])
    updatedPharmacies.push(pharmacy)
  }
}
