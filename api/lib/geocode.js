const rp = require('request-promise')
const { getParsedLocations } = require('helpers')

const KEY = 'AIzaSyDkq_8fuYEw5jgyeIAaU792K6-LrG-S7eY'
const URL = 'https://maps.googleapis.com/maps/api/geocode/json'

const getLngLat = async (pharmacies) => {
  const addresses = pharmacies.map(p => p.address)

  const options = {
    method: 'GET',
    uri: URL,
    qs: {
      destinations: getParsedLocations(addresses),
      key: KEY
    },
    json: true
  }

  res = await rp(options)

  return res
}
