const express = require('express')
const cors = require('cors')
const getPharmacies = require('./lib/site-scraper')
const { addGeolocation, addLngLat } = require('./lib/geolocate')
const { sortBy } = require('./lib/helpers')

const port = 3000
const app = express()
app.use(cors())

app.get('/api', async (req, res) => {
  let pharmacies;

  try {
    pharmacies = await getPharmacies()
  }
  catch (e) {
    console.log('Got pharmacies error', e.message) 
  }

  await addGeolocation(pharmacies)

  pharmacies.filter((p) => {
    return p.duration !== undefined && p.distance !== undefined
  })

  sortBy(pharmacies, 'duration')

  console.log(pharmacies)
  pharmacies = pharmacies.slice(0, 5)

  await addLngLat(pharmacies)

  res.send(pharmacies)
})

app.listen(port, () => console.log(`Magic is happening on port ${port}!`))
